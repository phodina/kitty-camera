# Kitty Camera
This camera caught my eye during the visit to ShenZen. Since my job is to build
a biometric camera I was interested what would be inside the device.

![Kitty camera](media/kitty_camera.jpg)

## Disassembly
In order to open the device the outer silicone cover has to be removed by
pulling it to the side.
After that 4 Phillips screws (PH000) need to be removed from each corner of the
back side of camera.
Special attention needs to be taken as there is a camera sensor attached to
the front side of the camera device along with a speaker.

![Disassembly](media/disassembly.jpg)
![Motherboard - Processor](media/motherboard_top.jpg)
![Motherboard - Battery](media/motherboard_bottom.jpg)

## Device enumeration
Here's the output of `dmesg` when the device is attached to the PC host.
```
[11170.100287] usb 1-5.3: new high-speed USB device number 13 using xhci_hcd
[11170.249000] usb 1-5.3: New USB device found, idVendor=1b3f, idProduct=2002, bcdDevice= 1.00
[11170.249015] usb 1-5.3: New USB device strings: Mfr=1, Product=2, SerialNumber=0
[11170.249021] usb 1-5.3: Product: GENERAL - UVC
[11170.249025] usb 1-5.3: Manufacturer: GENERAL
[11170.250416] usb 1-5.3: Found UVC 1.00 device GENERAL - UVC  (1b3f:2002)
[11170.250701] usb 1-5.3: UVC non compliance - GET_DEF(PROBE) not supported. Enabling workaround.
[11170.293291] usbcore: registered new interface driver snd-usb-audio

Bus 001 Device 013: ID 1b3f:2002 Generalplus Technology Inc. 808 Camera
```
